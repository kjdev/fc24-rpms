Fedora 24 - RPM files
=====================

Repository Setup
----------------

Install with dnf repository.

```
$ sudo cat > /etc/yum.repos.d/kjdev.repo <<EOF
[kjdev]
name=Fedora $releasever - kjdev
baseurl=https://bitbucket.org/kjdev/fc$releasever-rpms/raw/master/RPMS/
enabled=1
gpgcheck=0

[kjdev-scl]
name=Fedora $releasever - SCL - kjdev
baseurl=https://bitbucket.org/kjdev/fc$releasever-rpms/raw/master/SCL/RPMS/
enabled=0
gpgcheck=0
EOF
```

RPM Install
-----------

```
$ dnf install <PACKAGEs>
```
